# Manual De Usuario

- [Inicio de Sesión](#inicio-de-sesión)
- [Home](#home)
- [Catálogo de películas](#catálogo-de-plículas)
- [Renta de películas](#renta-de-películas)
- [Información y registro de tarjetas](#información-y-registro-de-tarjetas)
- [Historial](#historial)

## Descripción

blockbusted es una aplicación que le permitirá al usuario realizar rentas de películas de una forma rápida y eficiente. Podrá elegir entre en una gran cantidad de películas y rentarlas facilmente. Deberá crear una cuenta y a partir de ahí elegir entre varias opciones que le permitirán buscar una película, administrar sus tarjetas de pago, rentar películas, ver historial de transacciones, entre otras cosas. A continuación se realizará una descripción de todas las partes del sistema y su explicación.
## Inicio de Sesión


<div align="center">
    <img src="https://res.cloudinary.com/dst1u4bij/image/upload/v1651460799/login_nat28o.jpg" width="500">
    <p align="center">Login</p>
</div>

El usuario deberá ingresar su nombre de usuario y contraseña para acceder a su cuenta, de no tener una cuenta deberá crear una en la opción de create account.

<div align="center">
    <img src="https://res.cloudinary.com/dst1u4bij/image/upload/v1651461047/register_w9hk6c.jpg" width="500">
    <p align="center">Signup</p>
</div>

Al ingresar en la opción de "create account" accederá a un formulario donde debe ingresar los datos que se le solicitan para crerar su cuenta y terminara dando click en el botón de "create account".

## Home
<div align="center">
    <img src="https://res.cloudinary.com/dst1u4bij/image/upload/v1651461269/home_x8yche.jpg" width="500">
    <p align="center">Home</p>
</div>

Al iniciar sesión accederá a una página principal en donde tendrá un panel lateral con muchas opciones que se explicarán a continuación, en la parte inferior encontrará un botón rojo para cerrar sesión y regresar al login.

## Catálogo de plículas
<div align="center">
    <img src="https://res.cloudinary.com/dst1u4bij/image/upload/v1651461486/movies_ivybue.jpg" width="500">
    <p align="center">Movies</p>
</div>

## Renta de películas
Al ingresar en la opción "Movies" podrá observar esta página, en donde se le mostrarán todas las películas disponibles para rentar.

<div align="center">
    <img src="https://res.cloudinary.com/dst1u4bij/image/upload/v1651461688/rental_togpmm.jpg" width="500">
    <p align="center">Rentals</p>
</div>

Al ingresar en la opción "Rentals" accederá a está página donde aparecerán todas las películas disponibles junto a su precio y la opción de realizar una renta rápida, o agregar al carrito.

<div align="center">
    <img src="https://res.cloudinary.com/dst1u4bij/image/upload/v1651462384/rental2_avagel.jpg" width="500">
    <p align="center">rentals</p>
</div>

Al agregar al carrito le aparecerán las peliculas del lado derecho junto con la opción de incrementar o decrementar la cantidad de unidades que se desea adquirir, además de la opción de remover del carrito. En la parte inferior está la opción de vaciar completamente la renta o ir a los detalles de pago en el botón "go to checkout".

<div align="center">
    <img src="https://res.cloudinary.com/dst1u4bij/image/upload/v1651462621/checkout_nnd2ow.jpg" width="500">
    <p align="center">checkout</p>
</div>

Al ingresar a la opción de "chekout" aparecerá en la parte izquierda las películas que se desea rentar, junto con su precio, del lado derecho podrá observar las tarjetas disponibles para realizar el pago, en donde deberá seleccionar una, y en la parte inferior se confirman los datos de la tarjeta y el monto a pagar y el botón de "process payment" que realizará el pago y la renta de la película.

## Información y registro de tarjetas
<div align="center">
    <img src="https://res.cloudinary.com/dst1u4bij/image/upload/v1651463035/payment_y8vy7w.jpg" width="500">
    <p align="center">payment</p>
</div>

Al ingresar en la opción de "Payment Information" accederá a esta página donde podrá observar del lado derecho el informe de todas las tárjetas que se han registrado en la plataforma y en el costado izquiero un formulario para agregar una nueva tarjeta.

## Historial
<div align="center">
    <img src="https://res.cloudinary.com/dst1u4bij/image/upload/v1651463212/record_ari59l.jpg" width="500">
    <p align="center">record</p>
</div>

En la opción de "Record" podrá acceder a un historial de los movimientos realizados, tales como rentas, devoluciones y pagos.

