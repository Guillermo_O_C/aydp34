import { AppRoute } from "../interfaces/interfaces";
import { CardsPage } from "../pages/CardsPage";
import { HomePage } from "../pages/HomePage";
import { MoviesPage } from "../pages/MoviesPage";
import { RentalPage } from '../pages/RentalPage';
import { RecordPage } from '../pages/RecordPage';
import { InventoryPage } from "../pages/InventoryPage";

export const routes: AppRoute[] = [
    {
        name: 'Home',
        path: '/user/home',
        component: HomePage
    },
    {
        name: 'Catalog',
        path: '/user/movies',
        component: MoviesPage,
    }
    ,
    {
        name: 'Rentals',
        path: '/user/rentals',
        component: RentalPage,
    },
    {
        name: 'My Movies',
        path: '/user/mymovies',
        component: InventoryPage,
    },
    {
        name: 'Payment Information',
        path: '/user/payment-info',
        component: CardsPage,
    },
    {
        name: 'Record',
        path: '/user/record',
        component: RecordPage,
    }
]
