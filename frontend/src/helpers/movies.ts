
export interface Movie {
    movieid: number;
    title: string;
    image: string;
    price?: number;
    selected?: boolean;
}

export const movies: Movie[] = [
    {
        movieid: 1,
        title: 'Space jam 2',
        image: 'https://www.ecartelera.com/carteles/13100/13120/002_m.jpg',
        price: 100
    },
    {
        movieid: 2,
        title: 'Spiderman',
        image: 'https://img.ecartelera.com/noticias/fotos/66900/66992/2.jpg',
        price: 150
    },
    {
        movieid: 3,
        title: 'The Batman',
        image: 'https://img.ecartelera.com/noticias/fotos/62200/62242/1.jpg',
        price: 200
    },
    {
        movieid: 4,
        title: 'Sonic 2',
        image: 'https://www.diariodevenusville.com/wp-content/uploads/2022/03/sonic_the_hedgehog_two_ver24.jpg',
        price: 115
    },
    {
        movieid: 5,
        title: 'Doctor Strange',
        image: 'https://es.web.img3.acsta.net/pictures/22/04/07/11/43/1839314.jpg',
        price: 200
    }
];
