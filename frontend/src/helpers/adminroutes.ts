import { AdminHomePage } from "../adminPages/AdminHomePage";
import { AdminTransactionsPage } from "../adminPages/AdminTransactionsPage";
import { AppRoute } from "../interfaces/interfaces";

export const adminRoutes: AppRoute[] = [
    {
        name: 'Home',
        path: 'home',
        component: AdminHomePage
    },
    {
        name: 'Transactions',
        path: 'transactions',
        component: AdminTransactionsPage
    },
]

