
import React, { useState, createContext } from 'react'
import { useNavigate } from 'react-router-dom'
import axios from 'axios';
import Swal from 'sweetalert2'


interface initState {
  uid: string;
  checking: boolean;
  logged: boolean;
  nombreUsuario: string;
  usuario: string;
  fotoPerfil: string;
  email: string;
  age: number;
  apellidoUsuario: string;
  dpi: string;
  // ADMIN
  isAdmin: boolean;
}

interface IContextProps {
  auth: initState;
  login: (usuario: string, claveUsuario: string) => Promise<void>;
  logout: () => boolean;
  register: (names: string, lastNames: string, username: string, email: string, dpi: string, age: number, password: string) => Promise<void>;
  updateUser: (email: string, username: string) => Promise<void>
}
export const AuthContext = createContext({} as IContextProps);

const initialState = {
  uid: '',
  checking: true,
  logged: false,
  isAdmin: false,
  nombreUsuario: '',
  usuario: '',
  fotoPerfil: '',
  email: '',
  age: 0,
  apellidoUsuario: '',
  dpi: ''

}

export const AuthProvider = ({ children }: any) => {

  const [auth, setAuth] = useState(initialState);

  const register = async (
    names: string,
    lastNames: string,
    username: string,
    email: string,
    dpi: string,
    age: number,
    password: string) => {

    await axios.post(`${process.env.REACT_APP_BACKEND}/signup`, {
      email,
      password,
      names,
      lastNames,
      dpi,
      age,
      username
    }).then((response) => {
      console.log(response.data)
      console.log('xd')
    })
      .catch((err) => {
        console.log(err);
      })

  }

  const login = async (usuario: string, claveUsuario: string) => {

    if (usuario === 'admin') {
      //console.log('admin')
      setAuth({
        ...initialState,
        uid: '0',
        nombreUsuario: '',
        usuario: 'admin',
        fotoPerfil: '',//usuario.fotoPerfil,
        logged: false,
        isAdmin: true,
        checking: false
      })

      //return auth.isAdmin;

    } else {
      // backend....
      await axios.post(`${process.env.REACT_APP_BACKEND}/login`, { usuario, claveUsuario })
        .then((response) => {

          // console.log(response.data.usuario)
          const { usuario } = response.data;

          if (usuario) {
            setAuth({
              uid: usuario.idUsuario,
              nombreUsuario: usuario.nombreCompleto,
              apellidoUsuario: usuario.apellido,
              usuario: usuario.nombreUsuario,
              email: usuario.email,
              age: usuario.age,
              dpi: usuario.dpi,
              fotoPerfil: '',//usuario.fotoPerfil,
              logged: true,
              isAdmin: false,
              checking: false
            })
          } else {
            Swal.fire('Error', 'Credentials not valid', 'error')

          }


        })
        .catch((err) => {
          Swal.fire('Error', 'Credentials not valid', 'error')
          console.log(err);
          setAuth({
            ...initialState,
            logged: false,
            isAdmin: false
          })
        })
    }



    //return auth.logged;

  }

  const logout = () => {

    // if(auth.isAdmin) {
    //   setAuth({
    //     ...initialState,
    //     logged: false,
    //     isAdmin: false
    //   })

    // }

    // backend....

    setAuth({
      ...initialState,
      logged: false,
      isAdmin: false
    })

    return auth.isAdmin;

  }


  const updateUser = async (email: string, username: string) => {

    let id = auth.uid;

    await axios.put(`${process.env.REACT_APP_BACKEND}/usuario`, 
      { id, email, username } ).then((response) => {

        if(response.data.correcto) {
          Swal.fire('Success', 'User updated', 'info')
        }else {
          Swal.fire('Error', 'User not updated ', 'error')
        }

      }).catch((error) => {
        console.log(error);
        Swal.fire('Error', 'An error has occured', 'error')
      })


  }


  return (
    <AuthContext.Provider value={{
      auth,
      register,
      login,
      logout,
      updateUser
    }}>
      {children}
    </AuthContext.Provider>
  )
}
