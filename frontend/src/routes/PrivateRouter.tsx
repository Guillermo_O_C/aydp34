import React from 'react';
import { Navigate } from 'react-router-dom';

interface Props {
    isAuthenticated: boolean;
    isAdmin: boolean;
    children: JSX.Element; // ReactComponent
}
export const PrivateRouter = ({ isAuthenticated, isAdmin, children }: Props) => {
    console.log(isAuthenticated)


    // * authenticated as client
    if (isAuthenticated) {
        return <Navigate to="/user/home" />
    }
    // * authenticated as admin 
    else if (isAdmin) {
        return <Navigate to="/admin/home" />
    }
    else {
        return children
    }

    // !return isAuthenticated ? <Navigate to="/home" /> : children;
};
