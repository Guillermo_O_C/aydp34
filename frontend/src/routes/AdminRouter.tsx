import React from 'react'
import { AdminSidebar } from '../adminComponents/AdminSidebar'

export const AdminRouter = () => {
    return (
        <div className="limiter">
            <div className="container-login100">
                <div className="wrap-login100 p-t-50 p-b-90">

                    <AdminSidebar />

                </div>
            </div>
        </div>
    )
}
