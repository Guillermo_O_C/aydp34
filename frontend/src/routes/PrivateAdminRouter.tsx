import React from 'react';
import { Navigate } from 'react-router-dom';


interface Props {
    isAdmin: boolean;
    children: JSX.Element; // ReactComponent
}

export const PrivateAdminRouter = ({ isAdmin, children }: Props) => {
    console.log('Hola')
    return isAdmin ? children : <Navigate to="/auth/login" />;
};
