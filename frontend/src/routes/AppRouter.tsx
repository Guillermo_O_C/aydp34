import React, { useContext } from 'react'
import { BrowserRouter as Router, Routes, Route, Navigate } from 'react-router-dom'
import { PublicRoute } from './PublicRouter';
import { PrivateRouter } from './PrivateRouter';
import { AuthRouter } from './AuthRouter';
import { UserRouter } from './UserRouter';
import { AuthContext } from '../context/AuthContext';
import { PrivateAdminRouter } from './PrivateAdminRouter';
import { AdminRouter } from './AdminRouter';

export const AppRouter = () => {

  const { auth } = useContext(AuthContext);
  console.log('logged ', auth.logged);
  console.log('admin ', auth.isAdmin);
  return (
    <Router>
      <Routes>

      <Route path="/*" element={
          <PublicRoute isAuthenticated={auth.logged}>
            <UserRouter />
          </PublicRoute>
        } />
        
        <Route path='/admin/*' element={
          <PrivateAdminRouter isAdmin={auth.isAdmin}>
            <AdminRouter />
          </PrivateAdminRouter>
        } />






        {/* el asterisco le dice a react que las rutas son anidadas y el elemento debe responder a la ruta que comienza con /auth/ */}
        <Route path="/auth/*" element={
          <PrivateRouter isAuthenticated={auth.logged} isAdmin={auth.isAdmin}>
            <AuthRouter />
          </PrivateRouter>
        } />

        <Route path="*" element={<h1>:/</h1>} />
        {/* <Route path="*" element={<Navigate to={<HomePage />} />} /> */}

      </Routes>
    </Router>
  )
}
