import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { Catalogo, Cart } from '../interfaces/interfaces';

export const useCatalog = () => {

    const [loadingMovies, setLoadingMovies] = useState(true);
    const [movieCatalog, setMovieCatalog] = useState<Catalogo[]>([])

    const getMovieCatalog = async () => {

        await axios.get(`${process.env.REACT_APP_BACKEND}/catalogo`)
            .then((resp) => {
                console.log(resp.data)

                if(resp.data.code === 0) {
                    setMovieCatalog(resp.data.catalogo);
                    setLoadingMovies(false);
                }

                
            })
            .catch((error) => {
                console.log(error);
                setLoadingMovies(true);
            })
    }


    useEffect(() => {

        getMovieCatalog()

    }, [])


    return {
        loadingMovies,
        movieCatalog
    }
}
