import axios from 'axios';
import React, { useState, useEffect, useContext } from 'react';
import Swal from 'sweetalert2';
import { AuthContext } from '../context/AuthContext';
import { Movie } from '../interfaces/interfaces';

export const useInventory = () => {

    const { auth } = useContext(AuthContext);

    const [loadingInventory, setLoadingInventory] = useState(true);
    const [userInventory, setUserInventory] = useState<Movie[]>([])

    const getUserInventory = async () => {

        await axios.get(`${process.env.REACT_APP_BACKEND}/inventario/${auth.uid}`)
            .then((resp) => {

                setUserInventory(resp.data.movies);
                setLoadingInventory(false)

            })
            .catch((err) => {
                console.log(err);
                setLoadingInventory(true)
            })

    }


    const deleteRent = async (movieId: number, idRent: number) => {

        let userId = auth.uid;

        await axios.post(`${process.env.REACT_APP_BACKEND}/deleteRent`, {
            userId,
            movieId,
            idRent
        })
            .then((resp) => {

                Swal.fire('Success', 'Movie returned', 'success')
                //setUserInventory(resp.data.movies);
                //setLoadingInventory(false)

            })
            .catch((err) => {
                console.log(err);
                Swal.fire('Error', 'An error has occurred', 'error')
                //setLoadingInventory(true)
            })
    }

    useEffect(() => {


        const interval = setInterval(() => {
            getUserInventory();
        }, 3500);
        getUserInventory();
        return () => clearInterval(interval);


    }, [loadingInventory])


    return {
        userInventory,
        loadingInventory,
        deleteRent
    }
}
