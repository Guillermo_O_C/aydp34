
import React, { useState, useEffect, useContext } from 'react';
import axios from 'axios'
import { Tarjeta } from '../interfaces/interfaces';
import { AuthContext } from '../context/AuthContext';
import Swal from 'sweetalert2';

export const useCards = () => {
    // * Get user data from context 
    const { auth } = useContext(AuthContext);

    // * Cards state
    const [loadingCards, setLoadingCards] = useState(true);
    const [userCards, setUserCards] = useState<Tarjeta[]>([])


    const getUserCards = async () => {

        await axios.get(`${process.env.REACT_APP_BACKEND}/tarjetas/${auth.uid}`)
            .then((resp) => {

                if (resp.data.code === 0) {

                    setUserCards(resp.data.tarjetas);
                    setLoadingCards(false);

                }
            })
            .catch((err) => {
                console.log(err);
            })

    }


    const addCard = async (cardNumber: string, accountAmount: number, expirationDate: string, ownersName: string, userId: number) => {

        await axios.post(`${process.env.REACT_APP_BACKEND}/add/card`, {
            cardNumber, accountAmount, expirationDate, ownersName, userId
        }).then((resp) => {
            console.log(resp.data)
        }).catch((err) => {
            console.log(err)
        })

    }

    const processPayment = async (amount: number, cardNumber: string) => {

        let userId = auth.uid;
        console.log(amount, cardNumber)
        await axios.post(`${process.env.REACT_APP_BACKEND}/payment`, {
            amount,
            cardNumber,
            userId
        }).then((response) => {

            console.log(response.data)
            Swal.fire('Success', 'Successful payment', 'success')

        }).catch((err) => {
            console.log(err)
            Swal.fire('Error', 'An error has occured with your payment', 'error')
        })
    }


    const rentCart = async (movieId: number) => {
    
        let userId = auth.uid;

        await axios.post(`${process.env.REACT_APP_BACKEND}/rentar`, {
            movieId,
            userId
        }).then((response) => {
            console.log(response);
        }).catch((error) => {
            console.log(error);
        })
    
    }
    useEffect(() => {

        //console.log(arregloRam.length)
        const interval = setInterval(() => {
            getUserCards();
        }, 3500);
        getUserCards();
        return () => clearInterval(interval);
    }, [])


    return {
        loadingCards,
        userCards,
        addCard,
        processPayment,
        rentCart
    }


}
