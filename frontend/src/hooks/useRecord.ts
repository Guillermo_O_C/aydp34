import React, { useEffect, useState } from 'react'
import axios from 'axios'
//import { baseURL } from '../helpers/apiurl';
import { useContext } from 'react';
import { AuthContext } from '../context/AuthContext';
import { Renta, Devolucion, Pago } from '../interfaces/interfaces';
import Swal from 'sweetalert2';

export const useRecord = () => {

    const { auth } = useContext(AuthContext)


    const [rentas, setRentas] = useState<Renta[]>();
    const [devoluciones, setDevoluciones] = useState<Devolucion[]>();
    const [pagos, setPagos] = useState<Pago[]>();





    const getRentas = async () => {

        await axios.get(`${process.env.REACT_APP_BACKEND}/historial/${auth.uid}`).then((response) => {

            setRentas(response.data.Rentas)
            setDevoluciones(response.data.Devoluciones)
            setPagos(response.data.Pagos)
        }).catch((err) => {

            console.log(err)
        })


    }



    useEffect(() => {

        const interval = setInterval(() => {
            getRentas()

        }, 2500);

        getRentas()


        return () => clearInterval(interval);


    }, [])



    return {
        getRentas,
        rentas,
        devoluciones,
        pagos

    }
}