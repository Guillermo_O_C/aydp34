import React from 'react'
import { Movie } from '../helpers/movies'
import { Catalogo, Cart } from '../interfaces/interfaces';


interface Props {
    movie: Catalogo;
    onRemove: (item: Catalogo) => void;
    onAddToCart: (item: Catalogo) => void;
    onSubToCart: (item: Catalogo) => void;
    getCuantity: (item: Catalogo) => number;
}

export const CartItem = ({ movie, onRemove, onAddToCart, onSubToCart, getCuantity }: Props) => {
    return (
        <>
            <div className="card text-dark" style={{ padding: '15px', borderRadius: '10px', width: '50%' }}>
                <h5>{movie.name}</h5>
                <small>${movie.unitPrice}</small>
                <small>{getCuantity(movie)}</small>

                <div className="row">
                    <div className="col-6">
                        <button className='btn btn-success mt-1' style={{ width: '100%' }} onClick={() => onAddToCart(movie)}>+</button>
                    </div>
                    <div className="col-6">
                        <button className='btn btn-danger mt-1' style={{ width: '100%' }} onClick={() => onSubToCart(movie)}>-</button>
                    </div>
                </div>


                <button className="btn btn-outline-danger mt-2" onClick={() => onRemove(movie)}>Remove</button>
                {/* <button className='btn btn-outline-primary' style={{ width: '20%' }}> + </button> */}
            </div>
            <br />
        </>
    )
}
