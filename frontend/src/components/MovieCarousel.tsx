import React, { useContext, useEffect, useRef, useState } from 'react'
import { motion } from 'framer-motion'
//import { movies } from '../helpers/movies';
import { Catalogo } from '../interfaces/interfaces';


interface Props {
    movies: Catalogo[];
}



export const MoviesCarousel = ({movies}: Props) => {

    const [width, setWidth] = useState(0);
    const carousel = useRef<any>();


    useEffect(() => {

        setWidth(carousel.current.scrollWidth - carousel.current.offsetWidth)

    }, [])

    return (
        <>

            {/* <small>{actualId}</small> */}
            <motion.div
                ref={carousel}
                className='carousel'
                whileTap={{ cursor: "grabbing" }}
                style={{ backgroundColor: '#fffbf3', borderRadius: '10px', height: '58vh' }}
            >

                <motion.div
                    drag='x'
                    dragConstraints={{ right: 0, left: -width }}
                    className='inner-carousel'>

                    { /** Movies */}
                    <>
                        {
                            movies.map((movie) => (
                                <motion.div className='item' style={{ height: '52vh', width: '30%' }} key={movie.id} >
                                    <img src={movie.image} alt="user" />
                                    <div className="center" style={{ border: 'none' }}>
                                        <b className='text-secondary mt-2'>{movie.name}</b>

                                    </div>

                                </motion.div>
                            ))
                        }

                    </>

                </motion.div>
            </motion.div>
        </>
    )
}