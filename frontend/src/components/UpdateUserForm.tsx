import React, { useContext } from 'react'
import { AuthContext } from '../context/AuthContext'
import { useForm } from '../hooks/useForm';

export const UpdateUserForm = () => {



    const { auth, updateUser } = useContext(AuthContext);
    console.log(auth)

    const { formData, onChangeForm } = useForm({
        email: auth.email,
        usuario: auth.usuario
        
    });


    const onUpdateUser = async() => {


        await updateUser(formData.email, formData.usuario);


    }

    return (
        <div className='card text-dark' style={{padding: '20px'}}>
            <div className="form-group">
                <label className="form-label mt-1">Email</label>
                <input
                    type="text"
                    className="form-control"
                    autoComplete='off'
                    name="email"
                    value={formData.email}
                    onChange={onChangeForm}
                />
            </div>

            <div className="form-group">
                <label className="form-label mt-1">DPI</label>
                <input
                    type="text"
                    className="form-control"
                    name="usuario"
                    readOnly={true}
                    autoComplete='off'
                    value={auth.dpi}
                //onChange={(ev) => onChangeForm(ev)}
                />
            </div>


            <div className="form-group">
                <label className="form-label mt-1">Age</label>
                <input
                    type="number"
                    className="form-control"
                    name="usuario"
                    autoComplete='off'
                    readOnly={true}
                    value={auth.age}
                //onChange={(ev) => onChangeForm(ev)}
                />
            </div>

            <div className="form-group">
                <label className="form-label mt-1">Username</label>
                <input
                    type="text"
                    className="form-control"
                    name="usuario"
                    autoComplete='off'
                    value={formData.usuario}
                    onChange={(ev) => onChangeForm(ev)}
                />
            </div>



            <div className="form-group d-grid gap-2 mt-3">
                <button className='btn btn-primary' onClick={() => onUpdateUser()} disabled={false}>Update user</button>
            </div>
        </div>
    )
}
