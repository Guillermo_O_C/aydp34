import React from 'react'
import { Movie } from '../helpers/movies'
import { Cart, Catalogo } from '../interfaces/interfaces';

interface Props {
    movie: Catalogo;
    handleClick: (item: Catalogo) => void
}

export const MovieCard = ({ movie, handleClick }: Props) => {
    return (
        <>
            <div className="card border-secondary mb-3" style={{ width: '100%' }}>
                <div className="card-header text-dark">
                    <b>{movie.name}</b>
                </div>
                <div className="card-body">
                    <h4 className="card-title text-dark">${movie.chargeRate}</h4>
                    
                    {/* <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p> */}
                    <button className="btn btn-primary" onClick={() => handleClick(movie)}>Add to cart</button>
                    <button className="btn btn-success" style={{ marginLeft: '10px' }}>Fast rental</button>

                </div>
            </div>
        </>
    )
}
