import React from 'react'
import { useForm } from '../hooks/useForm'
import { useContext } from 'react';
import { AuthContext } from '../context/AuthContext';
import Swal from 'sweetalert2';
import { useCards } from '../hooks/useCards';

export const AddCardForm = () => {

    const { auth } = useContext(AuthContext);

    const { addCard } = useCards();
    const { onChangeForm, formData } = useForm({
        ownersName: '',
        //lastName: '',
        cardNumber: 0,
        expirationDate: '4/26',
        securityCode: 0,
        accountAmount: 0
    });


    const onSubmitPayment = (ev: React.MouseEvent<HTMLFormElement, MouseEvent>) => {
        ev.preventDefault()
        console.log(formData);

        addCard(formData.cardNumber.toString(), formData.accountAmount, formData.expirationDate, formData.ownersName, parseInt(auth.uid))
        Swal.fire('Info', 'Card saved succesfully', 'info')
    }

    return (
        <>
            <div className="card border-secondary mt-2 text-dark"
                style={{
                    width: '60%'
                }}
            >
                <div className="card-header">
                    Payment Details
                </div>
                <div className="card-body">

                    <form onSubmit={onSubmitPayment} className='text-dark'>

                        {/** NAME */}
                        <div className="form-group">
                            <label className="form-label">
                                Full Name
                            </label>
                            <input
                                type="text"
                                className="form-control"
                                name="ownersName"
                                autoComplete='off'
                                value={formData.ownersName}
                                onChange={(ev) => onChangeForm(ev)}
                            />
                        </div>

                        {/** CARD NUMBER */}
                        <div className="form-group">
                            <label className="form-label">
                                Card number
                            </label>
                            <input
                                type="number"
                                className="form-control"
                                name="cardNumber"
                                autoComplete='off'
                                value={formData.cardNumber}
                                onChange={(ev) => onChangeForm(ev)}
                            />
                        </div>
                        {/** EXPIRATION DATE AND SECURITY CODE */}
                        <div className="form-group row">

                            {/* EXPIRATION DATE */}
                            <div className="col-6">

                                <label className='form-label'>
                                    Expiration date
                                </label>
                                <input
                                    type="text"
                                    className="form-control"
                                    name="expirationDate"
                                    autoComplete='off'
                                    value={formData.expirationDate}
                                    onChange={(ev) => onChangeForm(ev)}
                                />
                            </div>

                            {/* SECURITY CODE */}
                            <div className="col-6">

                                <label className='form-label'>
                                    Security code
                                </label>
                                <input
                                    type="number"
                                    className="form-control"
                                    name="securityCode"
                                    autoComplete='off'
                                    value={formData.securityCode}
                                    onChange={(ev) => onChangeForm(ev)}
                                />
                            </div>
                        </div>
                        <div className="form-group">
                            <label className="form-label">
                                Amount:
                            </label>
                            <input
                                type="number"
                                className="form-control"
                                name="accountAmount"
                                autoComplete='off'
                                value={formData.accountAmount}
                                onChange={(ev) => onChangeForm(ev)}
                            />
                        </div>
                        <div className="form-group d-grid gap-2 mt-3">
                            <button className='btn btn-primary'
                                //onSubmit={(ev) => onSubmitPayment(ev)}
                                disabled={false}>Add card</button>
                        </div>

                    </form>
                </div>
            </div>
        </>
    )
}
