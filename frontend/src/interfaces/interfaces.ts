import { LazyExoticComponent } from "react";

type JSXComponent = () => JSX.Element


export interface UserLogin {
    username: string;
    password: string;
}

export interface AppRoute {
    path: string;
    component: LazyExoticComponent<JSXComponent> | JSXComponent; //() => JSX.Element;
    name: string;
    children?: AppRoute[];
}

export interface Renta {
    id: number;
    fecha: string;
    name: string;
    image: string;
    chargeRate: number;
    active: number;

}

export interface Devolucion {
    id: number;
    fecha: string;
    name: string;
    image: string;
    chargeRate: number;
    active: number;
}

export interface Pago {
    idPayment: number;
    amount: number;
    payDate: Date;
    cardId: number;
    username: string;
}

// * Catalogo
export interface CatalogoResponse {
    mensaje: string;
    code: number;
    catalogo: Catalogo[];
}

export interface Catalogo {
    id: number;
    name: string;
    image: string;
    chargeRate: number;
    active: number;
    unitPrice: number;
    amount?: number;
}

export interface Cart extends Catalogo {
    amount: number;
}

// !--------------------

// * Tarjetas de credito
export interface CardsResponse {
    mensaje: string;
    code: number;
    tarjetas: Tarjeta[];
}

export interface Tarjeta {
    id: number;
    cardNumber: string;
    accountAmount: number;
    expirationDate: string;
    ownersName: string;
    userId: number;
    cardId: number;
}

// !--------------------


// * Inventory

export interface InventoryResponse {
    mensaje: string;
    movies: Movie[];
    code: number;
}

export interface Movie {
    id: number;
    idRent: number;
    username: string;
    name: string;
    image: string;
    fecha: Date;
}
