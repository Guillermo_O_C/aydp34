import React from 'react'
import { useInventory } from '../hooks/useInventory';
import { Movie } from '../interfaces/interfaces';

export const InventoryPage = () => {

    const { userInventory, loadingInventory, deleteRent } = useInventory();



    const onDeleteRent = async (movie: Movie) => {
        // movieId, idRent
        await deleteRent(movie.id, movie.idRent);
    }

    return (
        <div className='container-fluid float-container mt-5'>

            <h1>My Movies</h1>
            <hr />
            {
                loadingInventory ? (
                    <h3>Loading...</h3>
                ) :
                    (
                        <div className="card" style={{ width: '100%' }}>
                            <div className="card-body">
                                <table className="table table-hover text-dark">
                                    <thead>
                                        <tr>
                                            <th scope="col">Titulo</th>
                                            <th scope="col">Fecha</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        {
                                            userInventory.map((item) => (
                                                <tr className='table-active'>
                                                    <th className='text-dark'>{item.name}</th>
                                                    <td className='text-dark'>{item.fecha}</td>
                                                    <td>
                                                        <button className='btn btn-warning' onClick={() => onDeleteRent(item)}>Return</button>
                                                    </td>
                                                </tr>
                                            ))
                                        }


                                    </tbody>
                                </table>
                            </div>
                        </div>

                    )

            }


        </div>
    )
}
