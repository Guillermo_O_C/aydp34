import React, { useState } from 'react'
import { Movie } from '../helpers/movies';
import { useCards } from '../hooks/useCards';
import { Cart, Catalogo, Tarjeta } from '../interfaces/interfaces';
import Swal from 'sweetalert2';

interface Props {
    cart: Catalogo[];
    onToggleCheckout: () => void;
}

export const CheckoutPage = ({ cart, onToggleCheckout }: Props) => {

    const { loadingCards, userCards, processPayment, rentCart } = useCards();

    const [paymentCard, setPaymentCard] = useState<Tarjeta>();

    const onGetTotalCart = () => {

        let total = 0;
        cart.map((item) => {
            total += item.unitPrice!
        })

        return total;
    }

    const onSelectCard = (card: Tarjeta) => {

        setPaymentCard(card);

    }


    const onProcessPayment = async () => {


        // * Validar si la tarjeta tiene suficiente saldo
        if (onGetTotalCart() > paymentCard?.accountAmount!) {
            Swal.fire('Error', 'Insufficient card balance, choose another card', 'error')


        } else {

            onRentalCart();

            // * Procesar pago
            await processPayment(onGetTotalCart(), paymentCard?.cardNumber!);

            //

            //Swal.fire('Success', 'Successful payment', 'success')
        }
    }

    const onRentalCart = () => {

        cart.map(async (element) => {
            await rentCart(element.id);
        })

    }

    return (
        <div className='container'>
            <h1>Checkout</h1>
            <button className='btn btn-primary' onClick={onToggleCheckout}>Back</button>
            <hr />
            <div className="row">
                <div className="col-5">

                    <div className="card border-secondary mb-3" style={{ width: '100%' }}>
                        <div className="card-header text-dark">
                            My cart
                        </div>
                        <div className="card-body">
                            <table className="table table-hover text-dark">
                                <thead>
                                    <tr>
                                        <th scope="col">Code</th>
                                        <th scope="col">Movie</th>
                                        <th scope="col">Amount</th>
                                        <th scope="col">Unit. price</th>
                                        <th scope="col">Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        cart.map((item) => (
                                            <tr className='table-active'>
                                                <th className='text-dark'>{item.id}</th>
                                                <td className='text-dark'>{item.name}</td>
                                                <td className='text-dark'>{item.amount}</td>
                                                <td className='text-dark'>{(item.unitPrice / item.amount!).toFixed(2)}</td>
                                                <td className='text-dark'>${item.unitPrice}</td>
                                            </tr>
                                        ))
                                    }

                                </tbody>
                            </table>


                        </div>
                    </div>
                </div>

                <div className="col-7">

                    {/** PAYMENT FORM */}
                    <div className="card" style={{ width: '70%' }}>
                        <div className="card-header text-dark">
                            Select card
                        </div>
                        <div className="card-body">
                            <table className="table table-hover text-dark">
                                <thead>
                                    <tr>
                                        <th scope="col">Card number</th>
                                        <th scope="col">Amount</th>
                                        <th scope="col">Expiration date</th>
                                        <th scope="col">Select</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        loadingCards ?
                                            (
                                                <small>No cards</small>
                                            ) :
                                            (
                                                userCards.map((card) => (
                                                    <tr>
                                                        <td className='text-dark'>
                                                            XXXX-XXXX-XXXX-{card.cardNumber.slice(card.cardNumber.length - 4, card.cardNumber.length)}
                                                        </td>
                                                        <td className='text-dark'>
                                                            {card.accountAmount}
                                                        </td>
                                                        <td className='text-dark'>
                                                            {card.expirationDate}
                                                        </td>
                                                        <td>

                                                            <label className="form-check-label ">
                                                                <input type="radio"
                                                                    className="form-check-input"
                                                                    name="optionsRadios"
                                                                    id="optionsRadios1"
                                                                    value="option1"
                                                                    //checked={true} 
                                                                    onClick={() => onSelectCard(card)}
                                                                />

                                                            </label>

                                                        </td>
                                                    </tr>
                                                ))
                                            )
                                    }
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <>
                        {
                            paymentCard !== null && (
                                <div className='card text-dark mt-5' style={{ width: '70%' }}>


                                    <div className="card-header">
                                        <h4>Payment</h4>
                                    </div>
                                    <div className="card-body">

                                        <label>Card number</label>
                                        <pre>{paymentCard?.cardNumber}</pre>

                                        <label>Card owner</label>
                                        <pre>{paymentCard?.ownersName}</pre>

                                        <label>Expiration date</label>
                                        <pre>{paymentCard?.expirationDate}</pre>


                                        <h5 className='text-dark'>Total: <small>${onGetTotalCart()}</small></h5>
                                        <div className="form-group d-grid gap-2 mt-3">
                                            <button className='btn btn-success' onClick={onProcessPayment} disabled={false}>Process Payment</button>
                                        </div>
                                    </div>



                                </div>
                            )
                        }
                    </>
                </div>
            </div>


        </div>
    )
}
