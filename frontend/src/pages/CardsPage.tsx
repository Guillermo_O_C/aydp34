import React from 'react'
import { AddCardForm } from '../components/AddCardForm'
import { useCards } from '../hooks/useCards';

export const CardsPage = () => {

    const { loadingCards, userCards } = useCards();

    return (
        <div className='container float-container mt-5'>
            <h1>Payment information</h1>

            <hr />
            <div className="row container mt-5">
                <div className="col-6">

                    <h4>Add new Card</h4>
                    <AddCardForm />

                </div>

                <div className="col-6">
                    {/** Credit cards */}
                    <h4>My credit Cards</h4>
                    <div className="card" style={{ width: '100%' }}>
                        <div className="card-body">
                            <table className="table table-hover text-dark">
                                <thead>
                                    <tr>
                                        <th scope="col">Card number</th>
                                        <th scope="col">Amount</th>
                                        <th scope="col">Expiration date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        loadingCards ?
                                            (
                                                <small>No cards</small>
                                            ) :
                                            (
                                                userCards.map((card) => (
                                                    <tr>
                                                        <td className='text-dark'>
                                                            XXXX-XXXX-XXXX-{card.cardNumber.slice(card.cardNumber.length-4, card.cardNumber.length )}
                                                        </td>
                                                        <td className='text-dark'>
                                                            {card.accountAmount}
                                                        </td>
                                                        <td className='text-dark'>
                                                            {card.expirationDate}
                                                        </td>
                                                    </tr>
                                                ))
                                            )
                                    }
                                </tbody>
                            </table>
                        </div>
                    </div>



                </div>
            </div>



        </div>
    )
}
