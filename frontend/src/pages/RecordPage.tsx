import React, { useState, useContext } from 'react'
import { useRecord } from '../hooks/useRecord'

export const RecordPage = () => {

    const { rentas, devoluciones, pagos } = useRecord();

    //console.log(rentas)
    return (
        <div className='container-fluid float-container mt-5'>
            {

                <>
                    <h1>History</h1>
                    <hr />

                    <div className="row container">
                        <div className="col-12">

                            <div className="card border-secondary mb-3" style={{ width: '100%' }}>
                                <div className="card-header text-dark">
                                    <b>HISTORIAL DE RENTAS</b>
                                </div>
                                <div className="card-body">
                                    <table className="table">
                                        <tr>
                                            <th><h4 className="card-title text-dark">ID</h4></th>
                                            <th><h4 className="card-title text-dark">FECHA</h4></th>
                                            <th><h4 className="card-title text-dark">PELICULA</h4></th>
                                            <th><h4 className="card-title text-dark">CHARGE RATE</h4></th>

                                        </tr>
                                        {(rentas != undefined )&&(
                                            
                                            rentas.map((item) => (
                                                
                                                <>
                                                    <tr >

                                                        <td>
                                                            <h6 className="card-title text-dark">{item.id}</h6>
                                                        </td>
                                                        <td>
                                                            <h6 className="card-title text-dark">{item.fecha}</h6>
                                                        </td>
                                                        <td>
                                                            <h6 className="card-title text-dark">{item.name}</h6>
                                                        </td>
                                                        <td>
                                                            <h6 className="card-title text-dark">${item.chargeRate}</h6>
                                                        </td>
                                                    </tr>
                                                </>
                                            ))
                                         ) }

                                    </table>

                          
                                </div>
                            </div>

                        </div>


                    </div>



                    <div className="row container">
                        <div className="col-12">

                            <div className="card border-secondary mb-3" style={{ width: '100%' }}>
                                <div className="card-header text-dark">
                                    <b>HISTORIAL DE DEVOLUCIONES</b>
                                </div>
                                <div className="card-body">
                                    <table className="table">
                                        <tr>
                                            <th><h4 className="card-title text-dark">ID</h4></th>
                                            <th><h4 className="card-title text-dark">FECHA</h4></th>
                                            <th><h4 className="card-title text-dark">PELICULA</h4></th>
                                            

                                        </tr>
                                        {(devoluciones != undefined )&&(
                                            
                                            devoluciones.map((item) => (
                                                
                                                <>
                                                    <tr >

                                                        <td>
                                                            <h6 className="card-title text-dark">{item.id}</h6>
                                                        </td>
                                                        <td>
                                                            <h6 className="card-title text-dark">{item.fecha}</h6>
                                                        </td>
                                                        <td>
                                                            <h6 className="card-title text-dark">{item.name}</h6>
                                                        </td>

                                                    </tr>
                                                </>
                                            ))
                                         ) }

                                    </table>

                          
                                </div>
                            </div>

                        </div>


                    </div>


                    <div className="row container">
                        <div className="col-12">

                            <div className="card border-secondary mb-3" style={{ width: '100%' }}>
                                <div className="card-header text-dark">
                                    <b>HISTORIAL DE PAGOS</b>
                                </div>
                                <div className="card-body">
                                    <table className="table">
                                        <tr>
                                            <th><h4 className="card-title text-dark">ID</h4></th>
                                            <th><h4 className="card-title text-dark">FECHA</h4></th>
                                            <th><h4 className="card-title text-dark">MONTO</h4></th>
                                            <th><h4 className="card-title text-dark">NUMERO TARJETA</h4></th>
                                            

                                        </tr>
                                        {(pagos != undefined )&&(
                                            
                                            pagos.map((item) => (
                                                
                                                <>
                                                    <tr >

                                                        <td>
                                                            <h6 className="card-title text-dark">{item.idPayment}</h6>
                                                        </td>
                                                        <td>
                                                            <h6 className="card-title text-dark">{item.payDate}</h6>
                                                        </td>
                                                        <td>
                                                            <h6 className="card-title text-dark">{item.amount}</h6>
                                                        </td>
                                                        <td>
                                                            <h6 className="card-title text-dark">{item.cardId}</h6>
                                                        </td>
                                                    </tr>
                                                </>
                                            ))
                                         ) }

                                    </table>

                          
                                </div>
                            </div>

                        </div>


                    </div>
                </>

            }

        </div>
    )
}