import React from 'react'
import { UpdateUserForm } from '../components/UpdateUserForm'

export const HomePage = () => {

  return (
    <>
      <div className='container-fluid float-container mt-5'>
        {/* <Navbar /> */}
        <h1>Home Page</h1>
        <hr />

        {/** MOVIE CATALOG */}
        <div className="row">
          <h4> User data</h4>
          <div className="col-5">

             <UpdateUserForm />
          </div>

        </div>

      </div>
    </>

  )
}
