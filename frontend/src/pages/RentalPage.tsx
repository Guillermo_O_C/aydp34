import React, { useState } from 'react'
import { MovieCard } from '../components/MovieCard';
import { CartItem } from '../components/CartItem';
import { CheckoutPage } from './CheckoutPage';
import { useCatalog } from '../hooks/useCatalog';
import { Catalogo } from '../interfaces/interfaces';

export const RentalPage = () => {

    const [cart, setCart] = useState<Catalogo[]>([]);
    const { movieCatalog } = useCatalog();
    const [showCheckout, setShowCheckout] = useState(false);

    const handleClick = (item: Catalogo) => {
        //console.log(item)
        // Validar si la pelicula ya está en el cart 
        let a = cart.filter((movie) => movie.id === item.id);
        console.log(a.length);
        if (a.length === 0) {
            console.log('Se agrega al cart')
            item.amount! = 1;
            setCart([...cart, item])
        } else {
            console.log('No se agrega al cart ')
        }

        //
    }


    const handleRemoveCart = (item: Catalogo) => {
        console.log('element removed')

        const cartRemoved = cart.filter((movie) => movie.id !== item.id)

        setCart(cartRemoved)
    }

    const onGetTotal = () => {

        let total = 0;
        cart.map((movie) => {
            total = total + movie.unitPrice!
        })

        return total
    }

    const onToggleCheckout = () => {

        setShowCheckout(!showCheckout);

    }

    const handleAddToCart = (item: Catalogo) => {

        let temp = [...cart];

        let index = cart.findIndex(cartItem => cartItem.id === item.id);
        //console.log(index)
        console.log(item)
        temp[index].unitPrice = item.amount! * item.chargeRate;
        temp[index].unitPrice = item.unitPrice + temp[index].chargeRate
        temp[index].amount = temp[index].amount! + 1;

        console.log(temp)
        setCart(temp)


    }

    const handleSubToCart = (item: Catalogo) => {

        let temp = [...cart];

        let index = cart.findIndex(cartItem => cartItem.id === item.id);
        temp[index].unitPrice = item.amount! * item.chargeRate;
        
        if (temp[index].amount! === 1) {
            
            temp[index].unitPrice = 0;
            const cartRemoved = temp.filter((movie) => movie.id !== temp[index].id)
            console.log(cartRemoved.length)
            setCart(cartRemoved)
        } else {
            //console.log(index)
            //temp[index].unitPrice = item.amount! * item.chargeRate;
            temp[index].unitPrice = temp[index].unitPrice - temp[index].chargeRate
            temp[index].amount = temp[index].amount! - 1;
            console.log(temp)
            setCart(temp)
        }




    }


    const getCuantity = (item: Catalogo) => {

        return item.amount!;
    }
    const onEmptyCart = () => {

        setCart([]);
    }



    return (
        <div className='container float-container mt-5'>
            {
                (showCheckout) ?

                    <CheckoutPage cart={cart} onToggleCheckout={onToggleCheckout} />

                    : (
                        <>
                            <h1>RentalPage</h1>
                            <hr />

                            <div className="row container">
                                <div className="col-6">
                                    <h2>Select a movie</h2>
                                    {/* card */}
                                    {
                                        movieCatalog.map((movie) => (
                                            <MovieCard movie={movie} handleClick={handleClick} />
                                        ))
                                    }

                                </div>

                                <div className="col-6">
                                    <h2>Shopping cart</h2>
                                    {/*** CART */}
                                    {
                                        cart.map((item) => (
                                            <>
                                                <CartItem movie={item}
                                                    onRemove={handleRemoveCart}
                                                    onAddToCart={handleAddToCart}
                                                    onSubToCart={handleSubToCart}
                                                    getCuantity={getCuantity}
                                                />
                                            </>
                                        ))
                                    }

                                    <>
                                        <h5>Total: ${onGetTotal()}</h5>
                                        <button className="btn btn-primary"
                                            disabled={onGetTotal() === 0 ? true : false}
                                            onClick={onToggleCheckout}

                                        >
                                            Go to checkout
                                        </button>
                                        <button className="btn btn-danger"
                                            disabled={onGetTotal() === 0 ? true : false}
                                            onClick={onEmptyCart}
                                            style={{ marginLeft: '10px' }}
                                        >
                                            Empty cart
                                        </button>
                                    </>
                                </div>
                            </div>
                        </>
                    )
            }

        </div>
    )
}
