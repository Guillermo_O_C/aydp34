import React from 'react'
import { MoviesCarousel } from '../components/MovieCarousel'
import { Navbar } from '../components/Navbar'
import { useCatalog } from '../hooks/useCatalog';

export const MoviesPage = () => {

  const { loadingMovies, movieCatalog } = useCatalog();

  return (
    <div className='container-fluid float-container mt-5'>
      {/* <Navbar /> */}
      <h1>Movies Page</h1>
      <hr />


      {
        loadingMovies ? (
          <h4>Loading catalog...</h4>)
          : (
            <MoviesCarousel movies={movieCatalog} />
          )

      }

    </div>
  )
}
