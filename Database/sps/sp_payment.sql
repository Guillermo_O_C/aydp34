DELIMITER $$

CREATE PROCEDURE sp_payment
(
 i_amount decimal,
 i_userId int,
 i_cardNumber int,
 i_currencyId INT
)
BEGIN
    Declare card_amount     decimal;

    select accountAmount into card_amount from Card where cardNumber = i_cardNumber;

    IF card_amount > i_amount THEN
    
    INSERT INTO Payment(amount, userId, cardId, currencyId) VALUES ( i_amount, i_userId, i_cardNumber, i_currencyId);
    UPDATE Card SET accountAmount =  ( card_amount - i_amount ) WHERE cardNumber = i_cardNumber;

    select 'Pago realizado con éxito';


    ELSE
    select 'Falló Pago', card_amount;
    
    END IF;

END$$

DELIMITER ;
