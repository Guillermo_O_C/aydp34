select * from Currency;
select * from User;
select * from Card;


-- Create payment
insert into Payment(payDate, amount, userId, cardId, currencyId)
values('2022-05-03', 29.99, 2, 2, 1);

-- Get transaction
select u.username as Usuario, p.amount as Total, p.payDate as FechaPago
from Payment p join User u on u.id = p.userId
join Card c on c.id= p.cardId
join Currency cu on cu.idCurrency = p.currencyId
where u.id = 2;

