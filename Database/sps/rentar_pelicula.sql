DELIMITER $$

CREATE PROCEDURE renta_pelicula
(
 i_movieId int,
 i_userId int
)
BEGIN
    Declare flag_active boolean;

    select active into flag_active from Movie where id = i_movieId;

    IF flag_active = true THEN
    
    INSERT INTO Rent(movieId, userID) VALUES ( i_movieId, i_userId);
    select 'Pelicula Rentada Correctamente' 'Mensaje';


    ELSE
    
    select 'La película no se encuentra activa' 'Mensaje';
    
    END IF;

END$$

DELIMITER ;
