CREATE DATABASE IF NOT EXISTS BlockBusted;

USE BlockBusted;

-- table Movie
CREATE TABLE IF NOT EXISTS Movie(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT, 
    name VARCHAR(100) NOT NULL,
    image VARCHAR(1000) NOT NULL,
    chargeRate  DECIMAL(2) NOT NULL,
    active BOOLEAN NOT NULL DEFAULT TRUE 
);

-- table Language
CREATE TABLE IF NOT EXISTS Language(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT, 
    code VARCHAR(45) NOT NULL,
    description VARCHAR(100) NOT NULL
);

-- table MovieLanguage
CREATE TABLE IF NOT EXISTS MovieLanguage(
    movieId INT NOT NULL,
    languageId INT NOT NULL,
    FOREIGN KEY fk_movie_language(movieId) REFERENCES Movie(id),
    FOREIGN KEY fk_language_movie(languageId) REFERENCES Language(id)
);

-- table Availability
CREATE TABLE IF NOT EXISTS Availability(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT, 
    name VARCHAR(100) NOT NULL,
    serviceDays  INT NOT NULL,
    bonusDays INT NOT NULL, 
    fine DECIMAL(2) NOT NULL
);

-- table MovieAvailability
CREATE TABLE IF NOT EXISTS MovieAvailability(
    movieId INT NOT NULL,
    availabilityId INT NOT NULL,
    FOREIGN KEY fk_movie_availability(movieId) REFERENCES Movie(id),
    FOREIGN KEY fk_availability_movie(availabilityId) REFERENCES Availability(id)
);

-- table User
CREATE TABLE IF NOT EXISTS User(
    id INT  PRIMARY KEY NOT NULL AUTO_INCREMENT, 
    email VARCHAR(100) NOT NULL UNIQUE,
    password VARCHAR(100) NOT NULL ,
    names VARCHAR(100) NOT NULL,
    lastNames VARCHAR(100) NOT NULL,
    DPI VARCHAR(13) NOT NULL,
    age INT NOT NULL,
    username VARCHAR(20) NOT NULL UNIQUE
);

-- table UserKeys
CREATE TABLE IF NOT EXISTS UserKeys(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT, 
    clave VARCHAR(8) NOT NULL,
    movieId INT NOT NULL,
    userID INT NOT NULL,
    FOREIGN KEY fk_movie_key(movieId) REFERENCES Movie(id),
    FOREIGN KEY fk_user_key(userID) REFERENCES User(id)
);

-- table Rent
CREATE TABLE IF NOT EXISTS Rent(
    idRent INT PRIMARY KEY NOT NULL AUTO_INCREMENT, 
    fecha DATETIME DEFAULT CURRENT_TIMESTAMP,
    movieId INT NOT NULL,
    userID INT NOT NULL,
    FOREIGN KEY fk_movie_rent(movieId) REFERENCES Movie(id),
    FOREIGN KEY fk_user_rent(userID) REFERENCES User(id)
);

-- table Return
CREATE TABLE IF NOT EXISTS Retorno(
    idReturn INT PRIMARY KEY NOT NULL AUTO_INCREMENT, 
    fecha DATETIME DEFAULT CURRENT_TIMESTAMP,
    movieId INT NOT NULL,
    userID INT NOT NULL
    FOREIGN KEY fk_movie_renturn(movieId) REFERENCES Movie(id),
    FOREIGN KEY fk_user_renturn(userID) REFERENCES User(id)
);

-- table Card
CREATE TABLE IF NOT EXISTS Card(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT, 
    cardNumber VARCHAR(16) NOT NULL, 
    accountAmount DECIMAL NOT NULL,
    expirationDate VARCHAR(50) NOT NULL,
    ownersName VARCHAR(100) NOT NULL
);

-- table UserCard
CREATE TABLE IF NOT EXISTS UserCard(
    userId INT NOT NULL,
    cardId INT NOT NULL,
    FOREIGN KEY fk_user_card(userId) REFERENCES User(id),
    FOREIGN KEY fk_card_user(cardId) REFERENCES Card(id)
);

-- table UserInventory
use BlockBusted;
CREATE TABLE IF NOT EXISTS UserInventory(
    userId INT NOT NULL,
    movieId INT NOT NULL,
    FOREIGN KEY fk_user_inventory(userId) REFERENCES User(id),
    FOREIGN KEY fk_movie_inventory(movieId) REFERENCES Movie(id)
);

-- table Currency
CREATE TABLE IF NOT EXISTS Currency(
    idCurrency INT PRIMARY KEY NOT NULL AUTO_INCREMENT, 
    currency VARCHAR(45)
);

-- table Payment
CREATE TABLE IF NOT EXISTS Payment(
    idPayment INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    payDate DATETIME DEFAULT CURRENT_TIMESTAMP,
    amount DECIMAL(2) NOT NULL,
    userId INT NOT NULL,
    cardId INT NOT NULL,
    currencyId INT NOT NULL,
    FOREIGN KEY fk_user_payment(userId) REFERENCES User(id),
    FOREIGN KEY fk_card_payment(cardId) REFERENCES Card(id),
    FOREIGN KEY fk_currency_payment(currencyId) REFERENCES Currency(idCurrency)
);