const { Router } = require('express');
const { getHistorial, saveCreditCard , movies_catalog, getUserInventory, getTransactions, createPayment, makeRent, deleteRent} = require('../controllers/movies.controller');
const { login, signup, updateUser } = require('../controllers/user.controller');
const { addCard, get_cards, get_monedas} = require('../controllers/card.controller');
const router = Router();



router.post('/login', login);
router.post('/signup', signup);
router.post('/savecc', saveCreditCard);
router.get('/historial/:id', getHistorial);

//modificar usuario
router.put('/usuario', updateUser);

//agregar tarjeta
router.post('/add/card', addCard)

//obtener catalogo de peliculas disponibles
router.get('/catalogo', movies_catalog);

//obtener mis tarjetas, por id usuario
router.get('/tarjetas/:userId', get_cards);

//obtener monedas
router.get('/monedas', get_monedas);

// Inventario usuario
router.get('/inventario/:userId', getUserInventory);

// Obtener transacciones
router.post('/transactions', getTransactions);

// Crear pago
router.post('/payment', createPayment);

// Rentas
router.post('/rentar', makeRent)

// Borrar renta
router.post('/deleteRent', deleteRent);


module.exports = router
