const pool = require("./database.config");
const moment = require('moment');
const findUser = (usuario) => {
    return new Promise((resolve, reject) => {
        let query = `select * from User
        where username = '${usuario}';`;

        pool.query(query, (err, res) => {
            if (err) reject(err);
            return resolve(res);
        });
    });
}


const createUser = (userBody) => {
    let { email, password, names, lastNames, dpi, age, username } = userBody;
    return new Promise((resolve, reject) => {
        let query = `insert into User(email, password, names, lastNames, dpi, age, username)
        values ('${email}', '${password}', '${names}', '${lastNames}', '${dpi}', '${age}', '${username}' );`;
        pool.query(query, (err, res) => {
            if (err) reject(err);
            resolve(res);
        });
    });
}

const modificarUsuario = ( email, username, id ) => {
    return new Promise((resolve, reject) => {
        let query = `update User set email = '${email}' ,  username = '${username}' where id = ${id};`;
        pool.query(query, (err, res) => {
            if (err) reject(err);
            resolve(res);
        });
    });
}

const login = (userBody) => {
    let { usuario } = userBody;

    return new Promise((resolve, reject) => {
        let query = `SELECT * from User where username = '${usuario}';`
        pool.query(query, (err, res) => {

            if (err) reject(err);
            resolve(res);
        });
    });
}





const updateUserWithPhoto = ({ nombreUsuario, nombreCompleto, fotoPerfil, idUsuario }) => {

    return new Promise((resolve, reject) => {

        let query = `
        UPDATE Usuario SET nombreCompleto = '${nombreCompleto}', nombreUsuario = '${nombreUsuario}',fotoPerfil = '${fotoPerfil}' where idUsuario = ${idUsuario};
        `
        pool.query(query, (err, res) => {

            if (err) reject(err);
            resolve(res);
        });
    });

}

const updateUser = ({ nombreUsuario, nombreCompleto, idUsuario }) => {

    return new Promise((resolve, reject) => {

        let query = `
        UPDATE Usuario SET nombreUsuario = '${nombreUsuario}', nombreCompleto = '${nombreCompleto}' where idUsuario = ${idUsuario};
        `
        pool.query(query, (err, res) => {

            if (err) reject(err);
            resolve(res);
        });
    });

}

const getUsers = () => {

    return new Promise((resolve, reject) => {

        let query = `
        Select * from Usuario
        `
        pool.query(query, (err, res) => {

            if (err) reject(err);
            resolve(res);
        });
    });

}
const getUser = (id) => {

    return new Promise((resolve, reject) => {

        let query = `
        Select * from Usuario where idUsuario = ?
        `
        pool.query(query, [id], (err, res) => {

            if (err) reject(err);
            resolve(res[0]);
        });
    });

}

const getRent = (id) => {

    return new Promise((resolve, reject) => {

        let query = `
        select r.idRent as id, r.fecha, m.name, m.chargeRate FROM Rent r, Movie m, User u 
        where u.id = ${id} AND u.id = r.userID AND m.id = r.movieId
        `
        pool.query(query, [id], (err, res) => {

            if (err) reject(err);
            resolve(res);
        });
    });

}


const getReturn = (id) => {

    return new Promise((resolve, reject) => {

        let query = `
        select idReturn as id, fecha, Movie.name, Movie.image, Movie.chargeRate, Movie.active
        from Retorno
        inner join Movie on Retorno.movieId = Movie.id
        inner join User on Retorno.userID = User.id
        where User.id = ?
        order by fecha desc
        `
        pool.query(query, [id], (err, res) => {

            if (err) reject(err);
            resolve(res);
        });
    });

}

const getPayment = (id) => {

    return new Promise((resolve, reject) => {

        let query = `
        select c.idPayment,  c.amount, c.payDate, c.cardId, u.username
        FROM Payment c, User u
        WHERE u.id = c.userId and
        u.id = ${id};`
        pool.query(query, [id], (err, res) => {

            if (err) reject(err);
            resolve(res);
        });
    });

}

const saveCC = (cardNumber, accountAmount, expirationDate, ownersName) => {

    return new Promise((resolve, reject) => {
        let query = `insert into Card(cardNumber, accountMount, expirationDate, ownersName)
        values ('${cardNumber}', ${accountAmount}, '${expirationDate}', '${ownersName}');`;
        pool.query(query, (err, res) => {
            if (err) reject(err);
            resolve(res);
        });
    });
}


const addCard = (cardNumber, accountAmount, expirationDate, ownersName, userId) => {

    return new Promise((resolve, reject) => {

        let query = `call crea_tarjeta ('${cardNumber}',
        ${accountAmount}, '${expirationDate}',   '${ownersName}',  ${userId}
        );`;

        pool.query(query, (err, res) => {
            if (err) reject(err);
            resolve(res);
        });
    });
}


const catalogo_peliculas = () => {

    return new Promise((resolve, reject) => {
        let query = `select * from Movie where active = true;`;
        pool.query(query, (err, res) => {
            if (err) reject(err);
            resolve(res);
        });
    });
}

const getCards = (userId) => {
    return new Promise((resolve, reject) => {
        let query = `select * from Card c, UserCard uc where
         uc.userId = ${userId} and c.id = uc.cardId;`;
        pool.query(query, (err, res) => {
            if (err) reject(err);
            resolve(res);
        });
    });
}

const getCurrency = () => {
    return new Promise((resolve, reject) => {
        let query = `select * from Currency;`;
        pool.query(query, (err, res) => {
            if (err) reject(err);
            resolve(res);
        });
    });
}

const getUserInventory = (userId) => {
    return new Promise((resolve, reject) => {

        let query = `
        select m.id, r.idRent, u.username, m.name, m.image, r.fecha
        from Inventario r join User u on u.id = r.userID
        join Movie m on m.id= r.movieId
        where u.id = ${userId};
        `
        pool.query(query, (err, res) => {

            if (err) reject(err);
            resolve(res);
        });
    });
}

const createPayment = ({ amount, userId, cardId, cardNumber }) => {
    return new Promise((resolve, reject) => {
        let datetime = moment().format('YYYY-MM-DD  HH:mm:ss.000');

        console.log(datetime);
        let query = `
        call sp_payment(${amount}, ${userId}, ${cardNumber}, 1);
        `
        /**
         * insert into Payment(payDate, amount, userId, cardId, currencyId)
            values('${ datetime }', ${ amount }, ${ userId }, ${ cardId }, 1);
         * 
         */
        pool.query(query, (err, res) => {

            if (err) reject(err);
            resolve(res);
        });
    });
}

const getTransactions = (userId) => {

    return new Promise((resolve, reject) => {

        let query = `
            select u.username as Usuario, p.amount as Total, p.payDate as FechaPago
            from Payment p join User u on u.id = p.userId
            join Card c on c.id= p.cardId
            join Currency cu on cu.idCurrency = p.currencyId
            where u.id = ${userId};
        `
        pool.query(query, (err, res) => {

            if (err) reject(err);
            resolve(res);
        });
    });

}

const newRent = (userId, movieId) => {

    return new Promise((resolve, reject) => {

        let query = `
            call renta_pelicula(${movieId}, ${userId});
        `
        pool.query(query, (err, res) => {

            if (err) reject(err);
            resolve(res);
        });
    });

}



const deleteRent = (userId, movieId, idRent) => {

    return new Promise((resolve, reject) => {

        let query = `
            call sp_retorno(${userId}, ${movieId}, ${idRent})
        `
        pool.query(query, (err, res) => {

            if (err) reject(err);
            resolve(res);
        });
    });

}




var queries = {
    findUser,
    createUser,
    login,
    updateUserWithPhoto,
    updateUser,
    getUsers,
    getUser,
    getRent,
    getReturn,
    getPayment,
    saveCC,
    addCard,
    getCards,
    catalogo_peliculas,
    getCurrency,
    createPayment,
    getUserInventory,
    getTransactions,
    newRent,
    deleteRent,
    modificarUsuario
}

module.exports = {
    queries
}
