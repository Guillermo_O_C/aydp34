const { addCard, get_cards, get_monedas } = require('../controllers/card.controller');
const { createPayment } = require('../controllers/movies.controller');
const { mockRequest, mockResponse } = require('./mock');

require('../controllers/card.controller');


describe('addCard', () => {
    test('should 400', async () => {
        const req = mockRequest({cardNumber:"2563254122583", accountAmount:Math.floor(Math.random() * 100), expirationDate:"09/24", ownersName:"test test", userId:"15"});
        const res = mockResponse();
        await addCard(req, res);
        expect(res.status).toHaveBeenCalledWith(400);
        expect(res.json).toHaveBeenCalledWith({
                mensaje: "Error al insertar tarjeta",
                code: 0
        });
    });
});


describe('createPayment', () => {
    test('should 400', async () => {
        const req = mockRequest({body:{ "amount":"10", "userId":"1", "cardId":"24", "cardNumber":"37571" }});
        const res = mockResponse();
        await createPayment(req, res);
        expect(res.status).toHaveBeenCalledWith(400);
        expect(res.json).toHaveBeenCalledWith({
            mensaje: 'Error al procesar pago.',
            code: 4
        })
        });
});


describe('get_cards', () => {
    test('should 400', async () => {
        const req = {params:{userId:""}};
        const res = mockResponse();
        await get_cards(req, res);
        expect(res.status).toHaveBeenCalledWith(400);
        expect(res.json).toHaveBeenCalledWith({
            mensaje: 'Error al obtener tarjetas',
            code: 4
        });
        });
});