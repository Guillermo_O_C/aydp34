const { login, signup } = require('../controllers/user.controller');
const { mockRequest, mockResponse } = require('./mock');

require('../controllers/user.controller');


describe('Login', () => {
    test('should 400', async () => {
        const req = mockRequest({usuario:'unknown', claveUsuario:'123'});
        const res = mockResponse();
        await login(req, res);
        expect(res.status).toHaveBeenCalledWith(400);
    });
});


describe('Signup', () => {
    test('should 400', async () => {
        const req = mockRequest({email:"josue@gmail.com", password:"123", names:"josue", lastNames:"ORELLANA", dpi:"2589632587", age:"22", username:"josue"});
        const res = mockResponse();
        await signup(req, res);
        expect(res.status).toHaveBeenCalledWith(400);
        expect(res.json).toHaveBeenCalledWith({ 
            mensaje: 'Error en el servidor. Contacte con el administrador.',
            correcto: false,
        });
    });
});