const { addCard, get_cards, get_monedas } = require('../controllers/card.controller');
const { createPayment } = require('../controllers/movies.controller');
const { mockRequest, mockResponse } = require('./mock');

require('../controllers/card.controller');


describe('addCard', () => {
    test('should 200', async () => {
        const req = mockRequest({cardNumber:Math.floor(Math.random() * 100000), accountAmount:Math.floor(Math.random() * 100), expirationDate:"09/24", ownersName:"test test", userId:"1"});
        const res = mockResponse();
        await addCard(req, res);
        expect(res.status).toHaveBeenCalledWith(200);
        });
});

/*describe('createPayment', () => {
    test('should 200', async () => {
        const req = mockRequest({body:{ "amount":10, "userId":2, "cardId":16, "cardNumber":323232 }});
        const res = mockResponse();
        await createPayment(req, res);
        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).toHaveBeenCalledWith({
            mensaje: 'Pago realizado con exito.'
        })
        });
});*/


describe('get_cards', () => {
    test('should 200', async () => {
        const req = {params:{userId:"1"}};
        const res = mockResponse();
        await get_cards(req, res);
        expect(res.status).toHaveBeenCalledWith(200);
        });
});