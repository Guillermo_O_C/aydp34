const { login, signup } = require('../controllers/user.controller');
const { mockRequest, mockResponse } = require('./mock');

require('../controllers/user.controller');


describe('Login', () => {
    test('should 200', async () => {
        const req = mockRequest({usuario:'josue', claveUsuario:'123'});
        const res = mockResponse();
        await login(req, res);
        expect(res.status).toHaveBeenCalledWith(200);
        });
});


describe('Signup', () => {
    test('should 200', async () => {
        const req = mockRequest({email:"test"+Math.floor(Math.random() * 100)+"@gmail.com", password:"123", names:"test", lastNames:"test", dpi:"01010101"+Math.floor(Math.random() * 100), age:"20", username:"test"+Math.floor(Math.random() * 100)});
        const res = mockResponse();
        await signup(req, res);
        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).toHaveBeenCalledWith({
            mensaje: 'Usuario registrado exitosamente.',
            correcto: true
        })
        });
});