const { response  } = require("express")
const { queries } = require("../db/queries");
const bcryptjs = require('bcryptjs');

const addCard  = async ( req, res = response ) => {

    let { cardNumber, accountAmount, expirationDate, ownersName, userId } = req.body;

    try {

        let results = await queries.addCard( cardNumber, accountAmount, expirationDate, ownersName, userId)

        res.status(200).json({
            mensaje: results,
            code: 0
        })

        let retornoRep = () => {
            let ejemplo = 'hola';
            return ejemplo
        }

        console.log(retornoRep);

    } catch (error) {
        console.log(error);
        return res.status(400).json({
            mensaje: "Error al insertar tarjeta",
            code: 0
        });
    }
}


const get_cards  = async ( req, res = response ) => {

    let { userId } = req.params;
    try{
        let tarjetas = await queries.getCards(userId);

        if (tarjetas.length > 0){
            res.status(200).json({
                mensaje: 'Tarjetas Obtenidas con éxito',
                code: 0,
                tarjetas: tarjetas
            });
        }else{
            res.status(200).json({
                mensaje: 'No hay tarjetas registradas',
                code: 2,
                userdId: tarjetas
            });
        } ;

        let retornoRep = () => {
            let ejemplo = 'hola';
            return ejemplo
        }

        console.log(retornoRep);


    }catch (error) {
        console.log(error);
        return res.status(400).json({
            mensaje: 'Error al obtener tarjetas',
            code: 4
        });
    }
}


const get_monedas  = async ( req, res = response ) => {

    try{
        let currencies = await queries.getCurrency();

        if (currencies.length > 0){
            res.status(200).json({
                mensaje: 'Monedas Obtenidas con éxito',
                code: 0,
                tarjetas: currencies
            });
        }else{
            res.status(200).json({
                mensaje: 'No hay monedas registradas',
                code: 2,
                userdId: currencies
            });
        } ;

        let retornoRep = () => {
            let ejemplo = 'hola';
            return ejemplo
        }

        console.log(retornoRep);




    }catch (error) {
        console.log(error);
        return res.status(400).json({
            mensaje: 'Error al obtener monedas',
            code: 4
        });
    }
}


module.exports = {
    addCard,
    get_cards,
    get_monedas
}
