const { response, json } = require("express")
const { queries } = require("../db/queries");
const bcryptjs = require('bcryptjs');




const login = async (req, res = response) => {
    console.log(req.body)
    const { usuario, claveUsuario } = req.body;

    try {

        // Consulta para validar existencia del usuario
        let userExists = await queries.findUser(usuario);
        console.log(userExists)
        if (userExists.length === 0) {
            return res.status(400).json({
                mensaje: 'Usuario incorrecto.',
                status_login: false
            });
        }

        // Consulta login usuario
        let userBody = { usuario, claveUsuario };

        let loggedUser = await queries.login(userBody);

        let claveDB = loggedUser[0].password;
        //console.log('CLAVE BD:', claveDB)
        //console.log('CLAVE USUARIO', claveUsuario)
        if (claveDB !== claveUsuario) {
            return res.status(400).json({
                mensaje: 'Credenciales incorrectas',
                status_login: false
            });
        }

        // Verificar contraseña
        /*const validPassword = bcryptjs.compareSync( claveUsuario, claveDB );
        console.log('Valid passowrd', validPassword)
        if( !validPassword ) {
            return res.status(400).json({
                mensaje: 'Credenciales incorrectas',
                status_login: false
            });
        }*/

        res.status(200).json({
            mensaje: 'Login correcto',
            status_login: true,
            "usuario": {
                idUsuario: loggedUser[0].id,
                nombreCompleto: loggedUser[0].names,
                apellido: loggedUser[0].lastNames,
                nombreUsuario: loggedUser[0].username,
                email: loggedUser[0].email,
                age: loggedUser[0].age,
                dpi: loggedUser[0].DPI,
            }
        });



    } catch (error) {
        console.log(error)
        return res.status(400).json({
            mensaje: 'Error en el servidor. Contacte con el administrador.',
            correcto: false,
        });
    }

}

const updateUser = async (req, res = response) => {

    const { email, username, id } = req.body;

    try {

        let results = await queries.modificarUsuario(email, username, id);

        res.status(200).json({
            mensaje: 'Usuario modificado exitosamente.',
            correcto: true
        });

    } catch (error) {
        console.log(error);
        return res.status(400).json({
            mensaje: 'Error al modificar usuario.',
            correcto: false,
        });
    }
}

const signup = async (req, res = response) => {

    const { email, password, names, lastNames, dpi, age, username } = req.body;

    try {

        // Consulta para validar existencia del usuario
        let userExists = await queries.findUser(email);

        if (userExists.length !== 0) {
            return res.status(400).json({
                mensaje: 'El usuario ya se encuentra en uso.',
                correcto: false,
            })
        }

        // Encriptar contraseña
        // const salt = bcryptjs.genSaltSync(10);
        // hashedPassword = bcryptjs.hashSync( claveUsuario, salt );
        // console.log(hashedPassword);

        let userBody = { email, password, names, lastNames, dpi, age, username };

        let results = await queries.createUser(userBody);

        res.status(200).json({
            mensaje: 'Usuario registrado exitosamente.',
            correcto: true
        });

    } catch (error) {
        return res.status(400).json({
            mensaje: 'Error en el servidor. Contacte con el administrador.',
            correcto: false,
        });
    }
}




module.exports = {

    login,
    signup,
    updateUser
}
