const { response } = require("express")
const { queries } = require("../db/queries");
const bcryptjs = require('bcryptjs');

const saveCreditCard = async (req, res = response) => {

    let { cardNumber, accountAmount, expirationDate, ownersName } = req.body;

    try {

        let salt = bcryptjs.genSaltSync(10);
        let hashedCC = bcryptjs.hashSync(cardNumber, salt);

        console.log('Hashed CC:', hashedCC);

        let results = await queries.saveCC(hashedCC, accountAmount, expirationDate, ownersName)

        res.status(200).json({
            mensaje: 'Se ha guardado exitosamente la tarjeta de crédito.',
            correcto: true,
        })

    } catch (error) {
        console.log(error);
        return res.status(400).json({
            mensaje: 'Error en el servidor. Contacte con el administrador.',
            correcto: false,
        });
    }

}


const getHistorial = async (req, res = response) => {

    let userid = req.params.id;

    try {

        let rentas = await queries.getRent(userid)
        let retornos = await queries.getReturn(userid)
        let pagos = await queries.getPayment(userid)

        res.status(200).json({
            mensaje: 'HISTORIAL',
            status_login: true,
            "Rentas": rentas,
            "Devoluciones": retornos,
            "Pagos": pagos
        });



    } catch (error) {
        console.log(error)
        return res.status(400).json({
            mensaje: 'Error en el servidor. Contacte con el administrador.',
            correcto: false,
        });
    }

}


const movies_catalog = async (req, res = response) => {

    try {
        let catalogo = await queries.catalogo_peliculas();

        if (catalogo.length > 0) {
            res.status(200).json({
                mensaje: 'Catalogo obtenido con éxito',
                code: 0,
                catalogo: catalogo
            });
        } else {
            res.status(200).json({
                mensaje: 'Catalogo vacío',
                code: 2,
                catalogo: catalogo
            });
        };


    } catch (error) {
        return res.status(400).json({
            mensaje: 'Error al obtener catalogo de peliculas',
            code: 4
        });
    }
}

const getUserInventory = async (req, res = response) => {
    console.log(req.params)
    let { userId } = req.params;

    try {
        let mensaje = '';
        let movies = await queries.getUserInventory(userId);

        if (movies.length < 0) {
            mensaje = 'No se han rentado peliculas.'
        } else {
            mensaje = 'Inventario obtenido con exito';
        }

        return res.status(200).json({
            mensaje,
            movies,
            code: 2
        })


    } catch (error) {
        return res.status(400).json({
            mensaje: 'Error al obtener catalogo de peliculas',
            code: 4
        });
    }
}

const getTransactions = async (req, res = response) => {

    let { userId } = req.body
    try {

        let mensaje = '';
        let transactions = await queries.getTransactions(userId);
        console.log(userId);
        console.log('Transactions:', transactions);

        if (transactions.length < 0) {
            mensaje = 'No se han realizado transacciones.'
        } else {
            mensaje = 'Transacciones obtenidas con exito.';
        }

        return res.status(200).json({
            mensaje,
            transactions
        })

    } catch (error) {
        return res.status(400).json({
            mensaje: 'Error al obtener las transacciones',
            code: 4
        });
    }
}

const createPayment = async (req, res = response) => {

    try {
        console.log(req.body);
        let payment = await queries.createPayment(req.body);

        if (payment.length < 0) {
            return res.status(400).json({
                mensaje: 'Error al procesar el pago',
                code: 4
            });
        }

        return res.status(200).json({
            mensaje: 'Pago realizado con exito.'
        })

    } catch (error) {
        console.log(error);
        return res.status(400).json({
            mensaje: 'Error al procesar pago.',
            code: 4
        });
    }
}

const makeRent = async (req, res = response) => {

    const { userId, movieId } = req.body;
    try {

        await queries.newRent(userId, movieId)

        return res.status(200).json({
            mensaje: 'Renta realizado con exito.'
        })

    } catch (error) {
        console.log(error)
        return res.status(400).json({
            mensaje: 'Error al procesar renta.',
            code: 4
        });
    }

}


const deleteRent = async (req, res = response) => {

    const { userId, movieId, idRent } = req.body;
    try {
        
        await queries.deleteRent(userId, movieId, idRent);

        return res.status(200).json({
            mensaje: 'Renta eliminada'
        })

    } catch (error) {
        
        console.log(error)
        return res.status(400).json({
            mensaje: 'No se pudo eliminar',
            code: 4
        });
    }


}

module.exports = {
    getHistorial,
    saveCreditCard,
    movies_catalog,
    getUserInventory,
    getTransactions,
    createPayment,
    makeRent,
    deleteRent
}
