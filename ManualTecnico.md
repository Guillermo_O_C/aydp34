# Manual Técnico

- [Descripcion](#descripción)
- [Integracion Continua](#integración-continua)
- [Pruebas unitarias](#pruebas-unitarias)


## Descripción

Se realizó una aplicación web, cuya funcionalidad se basa en una aplicación para rentar películas, en la cual tenemos las funcionalidades de login, registro de usuarios, mostrar catálogo de películas rentar películas, ver hisotiral, entre otras. Para esta aplicación se realizó un servidor backend de nodejs del cual se obtuvieron los endpoints para obtener la información de la base de datos y enviarla al frontend, del mismo modo sirvió para insertar información en la base de datos a partir del frontend. Para el frontend se utilizó react y por medio del protocolo http se obtuvo la información del backend. Para la base de datos se utilizó el motor de MYSQL y se montó en una máquina virtual en la nube. Se realizaron pruebas unitarias de las distintas funcionalidades del sistema y se realizó integración continua para automatizar la compilación y las pruebas de código cada vez que un miembro del equipo confirma cambios en el control de versiones.
## Integración continua
---
### Jobs por Stage
<div align="center">
    <img src="https://res.cloudinary.com/dst1u4bij/image/upload/v1651467684/jobsStage_p3pa1u.jpg" width="500">
</div>

<div align="center">
    <img src="https://res.cloudinary.com/dst1u4bij/image/upload/v1651467698/jobsStage2_jjzgck.jpg" width="500">
</div>

### Stages

<div align="center">
    <img src="https://res.cloudinary.com/dst1u4bij/image/upload/v1651467697/stages_sclftt.jpg" width="500">
</div>

### VM con gitlab-runner

<div align="center">
    <img src="https://res.cloudinary.com/dst1u4bij/image/upload/v1651467698/vm_q3igri.jpg" width="500">
</div>

### Variables
<div align="center">
    <img src="https://res.cloudinary.com/dst1u4bij/image/upload/v1651467698/variables_dsidaw.jpg" width="500">
</div>

## Pruebas unitarias

### Failed Card
<div align="center">
    <img src="https://res.cloudinary.com/dst1u4bij/image/upload/v1651468519/failedcard_bqt2zr.jpg" width="500">
</div>

### Failed user
<div align="center">
    <img src="https://res.cloudinary.com/dst1u4bij/image/upload/v1651468525/faileduser_wve4cx.jpg" width="500">
</div>

### Successful card
<div align="center">
    <img src="https://res.cloudinary.com/dst1u4bij/image/upload/v1651468533/successfulcard_ayusyo.jpg" width="500">
</div>

### Successful user
<div align="center">
    <img src="https://res.cloudinary.com/dst1u4bij/image/upload/v1651468541/successfuluser_laaitl.jpg" width="500">
</div>

## Sonarqube
---
Para la integración de sonarqube al proyecto, se integró junto al pipeline CI/CD, justo antes de hacer el deploy.

Este permite analizar el código antes de hacer el despliegue, con la finalidad de validar si es óptimo sacarlo a producción o no.

Luego de cada cambio, el pipeline se encarga de realizar un análisis cuyo resultado se envían por correo eléctronico.

Se utilizó la versión en la nube de sonarqube, SonarCloud. Así es como se presenta el análisis cuando se realiza algún cambio:

<div align="center">
    <img src="imagenes/sonar2.png" width="500">
</div>

Al ingresar a inspeccionar las métricas, se puede visualizar lo siguiente:

<div align="center">
    <img src="imagenes/sonar1.png" width="500">
</div>

Este apartado indica cuántos bugs se presentaron en el análisis, así como código que puede optimizarse.
